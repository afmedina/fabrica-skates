# -*- coding: utf-8 -*-
from __future__ import print_function # para compatibilidade da função print com o Python 3
import random # gerador de números aleatórios
import simpy  # biblioteca de simulação
from pandas import DataFrame
from scipy.stats import t
from numpy import sqrt
import matplotlib.pyplot as plt

"""
Parâmetros iniciais
(depois joga para o Excel)
"""

procList = ['pressing', 'cutting', 'foundry', 'assemble', 'packing 1', 'packing 2']
demandRateDict = {'skates demand': 8, 'decks demand': 6, 'wheels demand': 6}
procTimeDict = {'pressing': 60.0, 'cutting': 80.0, 'foundry': 50.0, 'assemble': 30.0, 'packing 1': 15.0, 'packing 2': 12.0}
numResDict = {'pressing': 1, 'cutting': 1, 'foundry': 1, 'assemble': 1, 'packing 1': 1, 'packing 2': 1}

inventoryKeys = ['storage 1', 'storage 2', 'storage 3', 
                 'storage 1.tmp', 'storage 2.tmp', 'storage 3.tmp',
                 'storage 2a', 'storage 2b', 'storage 2a.tmp', 'storage 2b.tmp',
                 'storage 3a', 'storage 3b', 'storage 3a.tmp', 'storage 3b.tmp',
                 'skates', 'skates.tmp','decks', 'decks.tmp', 'wheels', 'wheels.tmp']
                 
initialInvDict = dict([(k, 0) for k in inventoryKeys])


class Param:
    simTime = 300.01*24*60  # total simulation time (days)
    numRep = 5              # number of replication
    warmupTime = 0          # warm-up time (days)
    log = False               # simulation log
    model = 5               # run model
    
def simDistributions(dist):
   return {
        'skates demand': random.randint(0,demandRateDict['skates demand']),
        'decks demand': random.randint(0, demandRateDict['decks demand']),
        'wheels demand': random.randint(0, demandRateDict['wheels demand']),
        'pressing': random.normalvariate(procTimeDict['pressing'], .1*procTimeDict['pressing']),
        'cutting': random.normalvariate(procTimeDict['cutting'], .1*procTimeDict['cutting']),
        'foundry': random.normalvariate(procTimeDict['foundry'], .1*procTimeDict['foundry']),
        'assemble': random.normalvariate(procTimeDict['assemble'], .1*procTimeDict['assemble']),
        'packing 1': random.normalvariate(procTimeDict['packing 1'], .1*procTimeDict['packing 1']),
        'packing 2': random.normalvariate(procTimeDict['packing 2'], .1*procTimeDict['packing 2']),
    }.get(dist, 0.0) 
   
class Process():
    def __init__(self, name, local, getKey=None, putKey=None, wait=False, nextProcess=None, route=None, end=None):
        self.pKey = name
        self.local = local
        self.getKey = getKey
        self.putKey = putKey
        self.wait = wait
        self.nextProcess = nextProcess
        self.route = None
        self.end = None
        
class Pedidos(object):
    def __init__(self, env, factory, hours=8):
        self.env = env         
        self.working = hours*60
        self.notWorking = 24*60 - self.working
        self.env.process(self.workingDay(factory))
        if Param.model == 1:
            self.env.process(self.model1(factory))
        elif Param.model == 2:
            self.env.process(self.model2(factory))
        elif Param.model == 3:
            self.env.process(self.model3(factory))
        elif Param.model == 4:
            self.env.process(self.model4(factory))
        elif Param.model == 5:
            self.env.process(self.model5(factory))
            
    def workingDay(self, factory):
          while True:
            factory.endDay = self.env.event()
            yield env.timeout(self.working)     # 1.440 min or 1 simulation day
            factory.endDay.succeed() 

    
    def model1(self, factory):
        cutting = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2', wait=True, end='end')
        pressing = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting)
        while True:
            for i in range(8):
                self.env.process(factory.pushProcess(pressing))
            yield factory.nextDay
            
    def model2(self, factory):
        cutting = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2', wait=True)        
        pressing = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting)
        foundry = Process('foundry', 'Wheel', putKey='storage 3', wait=True)
        assemble = Process('assemble', 'Skateboard', getKey=['storage 2', 'storage 3'], putKey='skates')
        
        self.env.process(factory.pullProcess(assemble))
        while True:
            for i in range(8):
                self.env.process(factory.pushProcess(pressing))
                self.env.process(factory.pushProcess(foundry))

            yield factory.nextDay

    def model3(self, factory):
        cutting_deck = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2a', wait=True)        
        cutting_skate = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2b', wait=True)                
        pressing_deck = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting_deck)
        pressing_skate = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting_skate)
        foundry_deck = Process('foundry', 'Wheel', putKey='storage 3a', wait=True)
        foundry_skate = Process('foundry', 'Wheel', putKey='storage 3b', wait=True)
        packing_1 = Process('packing 1', 'Deck', getKey=['storage 2a'], putKey='decks')
        packing_2 = Process('packing 2', 'Wheel', getKey=['storage 3a'], putKey='wheels')
        assemble = Process('assemble', 'Skateboard', getKey=['storage 2b', 'storage 3b'], putKey='skates')

        self.env.process(factory.pullProcess(packing_1))
        self.env.process(factory.pullProcess(packing_2))
        self.env.process(factory.pullProcess(assemble))
          
        while True:
            for i in range(6):
                self.env.process(factory.pushProcess(pressing_skate))
                self.env.process(factory.pushProcess(foundry_skate))
            
            for i in range(2):
                self.env.process(factory.pushProcess(pressing_deck))
            
            for i in range(3):
                self.env.process(factory.pushProcess(foundry_deck))
            
            yield factory.nextDay
            
    def model4(self, factory):
        yield factory.inventory['skates'].put(24)
        cutting = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2', wait=True)        
        pressing = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting)
        foundry = Process('foundry', 'Wheel', putKey='storage 3', wait=True)
        assemble = Process('assemble', 'Skateboard', getKey=['storage 2', 'storage 3'], putKey='skates')

        self.env.process(factory.pullProcess(assemble))
          
        while True:
            prodOrder = min(simDistributions('skates demand'), factory.inventory['skates'].level)
            if prodOrder > 0:
                yield factory.inventory['skates'].get(prodOrder)
            
                for i in range(prodOrder):
                    self.env.process(factory.pushProcess(pressing))
                    self.env.process(factory.pushProcess(foundry))

            yield factory.nextDay
            
    def model5(self, factory):
    
        yield factory.inventory['skates'].put(18)
        yield factory.inventory['decks'].put(6)
        yield factory.inventory['wheels'].put(6)
        demandRateDict['skates demand'] = 6
        demandRateDict['decks demand'] = 2
        demandRateDict['wheels demand'] = 3

        cutting_deck = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2a', wait=True)        
        cutting_skate = Process('cutting', 'Deck', getKey=['storage 1'], putKey='storage 2b', wait=True)                
        pressing_deck = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting_deck)
        pressing_skate = Process('pressing', 'Deck', putKey='storage 1', wait=True, nextProcess=cutting_skate)
        foundry_deck = Process('foundry', 'Wheel', putKey='storage 3a', wait=True)
        foundry_skate = Process('foundry', 'Wheel', putKey='storage 3b', wait=True)
        packing_1 = Process('packing 1', 'Deck', getKey=['storage 2a'], putKey='decks')
        packing_2 = Process('packing 2', 'Wheel', getKey=['storage 3a'], putKey='wheels')
        assemble = Process('assemble', 'Skateboard', getKey=['storage 2b', 'storage 3b'], putKey='skates')
        
        self.env.process(factory.pullProcess(packing_1))
        self.env.process(factory.pullProcess(packing_2))
        self.env.process(factory.pullProcess(assemble))
      
        while True:
            prodOrder = min(simDistributions('skates demand'), factory.inventory['skates'].level)
            if prodOrder > 0:
                yield factory.inventory['skates'].get(prodOrder)
                for i in range(prodOrder):
                    self.env.process(factory.pushProcess(pressing_skate))
                    self.env.process(factory.pushProcess(foundry_skate))
            
            prodOrder = min(simDistributions('decks demand'), factory.inventory['decks'].level)
            if prodOrder > 0:
                yield factory.inventory['decks'].get(prodOrder)
                for i in range(prodOrder):
                    self.env.process(factory.pushProcess(pressing_deck))
                
            prodOrder = min(simDistributions('wheels demand'), factory.inventory['wheels'].level)
            if prodOrder > 0:
                yield factory.inventory['wheels'].get(prodOrder)
                for i in range(prodOrder):
                    self.env.process(factory.pushProcess(foundry_deck))
    
            yield factory.nextDay
            
class Factory(object):
    def __init__(self, env):
        self.env = env
        self.nextDay, self.endDay = self.env.event(), self.env.event()

        self.WIPTotal = 0
        self.WIPList, self.InvList = [], []
        self.WIP = dict.fromkeys(procList, 0)
        self.queue = dict.fromkeys(procList, 0)
        self.procCount = dict.fromkeys(procList, 0)
        
        self.procRes = dict([(k, simpy.PriorityResource(env, capacity=numResDict[k])) for k in procList])
        self.inventory = dict([(k, simpy.Container(env, init=initialInvDict[k])) for k in inventoryKeys])

        self.env.process(self.monitor(.1*60))
        self.env.process(self.turnResOff())
        
    def turnResOff(self, hours=8):
        while True:
            yield self.endDay
            reqDict = dict([(res, res.request(priority=-1)) for res in self.procRes.values()])
            for res, req in reqDict.items():
                yield req
            yield self.nextDay
            for res, req in reqDict.items():
                yield res.release(req)
            
    def upWip(self, processKey):
    # incrementa o WIP
        self.queue[processKey] -= 1
        self.WIP[processKey] += 1
        if processKey == 'pressing':
            self.WIPTotal += 1
            
    def downWip(self, processKey):
    # incrementa o WIP
        self.procCount[processKey] += 1 
        self.WIP[processKey] -= 1 
            
    def pullProcess(self, currProcess):    
        while True:
            with self.procRes[currProcess.pKey].request(priority=0) as req:
                yield req
                if currProcess.putKey:
                    yield self.inventory[currProcess.putKey + '.tmp'].put(1)
                                
                if currProcess.getKey:
                    getInv = [self.inventory[k].get(1) for k in currProcess.getKey]
                    yield simpy.AllOf(self.env, getInv)

                    self.queue[currProcess.pKey] += 1
                    for k in currProcess.getKey:
                        self.inventory[k + '.tmp'].get(1)
                        self.logSimula(currProcess.pKey, (k + '-', currProcess.local))                    
                else:
                    self.queue[currProcess.pKey] += 1

                self.upWip(currProcess.pKey)
                self.logSimula(currProcess.pKey, ('START '+ currProcess.pKey, currProcess.local))
                yield env.timeout(simDistributions(currProcess.pKey))

            self.downWip(currProcess.pKey)
            self.logSimula(currProcess.pKey, ('END '+ currProcess.pKey, currProcess.local))
            if currProcess.wait == True:
                self.logSimula(currProcess.pKey, ('START WAIT '+ currProcess.pKey, currProcess.local))
                yield self.nextDay
                self.logSimula(currProcess.pKey, ('END WAIT '+ currProcess.pKey, currProcess.local))
            if currProcess.putKey:
                yield self.inventory[currProcess.putKey].put(1)
                self.logSimula(currProcess.pKey, (currProcess.putKey + '+', currProcess.local))
            self.WIPTotal -= 1
                    

    def pushProcess(self, currProcess):
        self.queue[currProcess.pKey] += 1
        with self.procRes[currProcess.pKey].request(priority=0) as req:
            yield req
            if currProcess.putKey:
                yield self.inventory[currProcess.putKey + '.tmp'].put(1)
            if currProcess.getKey:
                getInv = [self.inventory[k].get(1) for k in currProcess.getKey]
                yield simpy.AllOf(self.env, getInv)
                
                for k in currProcess.getKey:
                    self.inventory[k + '.tmp'].get(1)
                    self.logSimula(currProcess.pKey, (k + '-', currProcess.local))

            self.upWip(currProcess.pKey)
            self.logSimula(currProcess.pKey, ('START '+ currProcess.pKey, currProcess.local))
            yield env.timeout(simDistributions(currProcess.pKey))
            self.downWip(currProcess.pKey)
            self.logSimula(currProcess.pKey, ('END '+ currProcess.pKey, currProcess.local))
        
        if currProcess.wait == True:
            self.logSimula(currProcess.pKey, ('START WAIT '+ currProcess.pKey, currProcess.local))
            yield self.nextDay
            self.logSimula(currProcess.pKey, ('END WAIT '+ currProcess.pKey, currProcess.local))
            
        if currProcess.putKey:
            yield self.inventory[currProcess.putKey].put(1)
            self.logSimula(currProcess.pKey, (currProcess.putKey + '+', currProcess.local))
        
        if currProcess.nextProcess:
            self.env.process(self.pushProcess(currProcess.nextProcess))
        
        if currProcess.end:
            self.WIPTotal -= 1
            
    def monitor(self, interval=1440):
        while True:
            yield self.env.timeout(interval)
            self.WIPList.append((self.env.now/1440.0, self.WIPTotal))
            self.InvList.append((self.env.now/1440.0, self.inventory['skates'].level, self.inventory['decks'].level, self.inventory['wheels'].level))
              
    def logSimula(self, processKey, txt):
        if Param.log:
            print("{0:6.2f}   {1:20}   {2:10}  {3:6}   {4:6}   {5:6}".format(self.env.now/1440.0, txt[0][:20].upper(), txt[1][:12].upper(), self.queue[processKey], self.procCount[processKey],  self.WIPTotal ))
 

random.seed(1000)
plt.close("all")
fig = plt.figure()
ax = plt.subplot(111)
ax.grid(True)
#fig.canvas.set_window_title('Work in Progress')
fig.canvas.set_window_title('Inventory')

print('\n o/-<]: Simulate or Die Skate Factory  o\-<]: \n')
if Param.log:
    print("{0:^6}   {1:^20}   {2:^10}   {3:^6}   {4:^6}   {5:^6}".format('  Time', 'Event', 'Local', 'Queue', 'Count', 'WIPtot'))

resultsDF = DataFrame(columns=procList, index=['Replicação ' + str(i) for i in range(Param.numRep)])
for i in range(Param.numRep):
    env = simpy.Environment()
    factory = Factory(env)
    pedidos = Pedidos(env, factory)
    
    env.run(until=Param.simTime)
    #x, y = zip(*factory.WIPList)
    #ax.plot(x, y, label='Replication ' + str(i), linewidth=2.0)
    x, y, z, w = zip(*factory.InvList)
    ax.plot(x, y, label='Skates ', linewidth=2.0)
    ax.plot(x, z, label='Decks ', linewidth=2.0)
    ax.plot(x, w, label='Wheels ', linewidth=2.0)
    for j in procList:
        resultsDF.ix[i][j] = factory.procCount[j]

ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()
mean = resultsDF.mean()
stdDesv = resultsDF.std()
IC = t.ppf(0.975, Param.numRep-1)*stdDesv/sqrt(Param.numRep)
resultsDF.loc['Mean'] = mean
resultsDF.loc['Std Desv'] = stdDesv
resultsDF.loc['IC inf @ 95%'] = mean - IC
resultsDF.loc['IC sup @ 95%'] = mean + IC
print(resultsDF)

   