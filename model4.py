# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:56:14 2016

@author: afmedina2gmail.com
"""
# -*- coding: utf-8 -*-

import random # gerador de números aleatórios
import simpy  # biblioteca de simulação
from pandas import DataFrame
from scipy.stats import t
from numpy import sqrt
import matplotlib.pyplot as plt

from factory import Factory

demandRateDict = {'skates demand': 8, 'decks demand': 6, 'wheels demand': 6}

def demandRates(dist):
    # main demand distributions
    return {
        'skates demand': random.randint(0, demandRateDict['skates demand']),
        'decks demand': random.randint(0, demandRateDict['decks demand']),
        'wheels demand': random.randint(0, demandRateDict['wheels demand']),
    }.get(dist, 0.0)

class Param:
    # main simulation parameters
    simTime = 102.01*24*60  # total simulation time (days)
    numRep = 1              # number of replication
    warmupTime = 0          # warm-up time (days)
    trace = False           # simulation process trace
    model = 5               # run model
    
   
class Process():
    def __init__(self, factory, name, local, numRes=1, getKey=None, putKey=None, wait=False, nextProcess=None, route=None, begin=False, end=False):
        self.pKey = name
        self.local = local
        self.numRes = numRes
        self.getKey = getKey
        self.putKey = putKey
        self.wait = wait
        self.nextProcess = nextProcess
        self.route = None
        self.begin, self.end = begin, end
        factory.addProcess(self)
           
class Pedidos(object):
    def __init__(self, env, factory):
        self.env = env
        self.monitorList = []
        self.dailyDemand = 0
        self.env.process(self.createModel(factory)) # create and start model's simulations process
        self.env.process(self.monitor())            # start monitor simulation process
        self.env.process(factory.workingDay())          # start working schedulle simulation process
        for res in factory.procRes.values():
            self.env.process(factory.turnResOff(res))   # start open/closed simulation process
  
    def createModel(self, factory):
        cutting = Process(factory, 'cutting', 'Deck', getKey=['storage 1'], putKey='storage 2', wait=True)
        pressing = Process(factory,'pressing', 'Deck', putKey='storage 1', wait=True, begin=True, nextProcess=cutting)
        foundry = Process(factory,'foundry', 'Wheel', putKey='storage 3', wait=True)
        assemble = Process(factory,'assemble', 'Skateboard', getKey=['storage 2', 'storage 3'], putKey='skates', end=True)
        
        yield factory.inventory['skates'].put(24)

        demandRateDict['skates demand'] = 8

        self.env.process(factory.pullProcess(assemble))
        
        while True:
            self.dailyDemand = min(demandRates('skates demand'), factory.inventory['skates'].level)
            if self.dailyDemand > 0:
                yield factory.inventory['skates'].get(self.dailyDemand)
                for i in range(self.dailyDemand):
                    self.env.process(factory.pushProcess(pressing))
                    simpy.util.start_delayed(self.env, factory.pushProcess(foundry), 1440)

            yield factory.nextDay

    def monitor(self, interval=1440):
        while True:
            yield env.timeout(interval)
            self.monitorList.append((self.env.now/1440.0,
            factory.inventory['skates'].level,
            factory.WIPTotal, self.dailyDemand))


class Graph(object):
    def __init__(self, title):
        self.title = title
        self.fig = plt.figure()
        self.ax = plt.subplot(111)
        self.ax.grid(True)
        self.fig.canvas.set_window_title(self.title)
        
    def plotPlt(self, data):
        x, y, z, d = zip(*data)
        #d1, d2, d3 = zip(*d)
        self.ax.plot(x, y, label='Skates inventory', linewidth=2.0)
        self.ax.plot(x, z, label='WIP', linewidth=2.0)
        self.ax.plot(x, d, label='Skates demand', linewidth=2.0)
        self.ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.show()
    
def printResults(resultsDF):
    mean = resultsDF.mean()
    stdDesv = resultsDF.std()
    IC = t.ppf(0.975, Param.numRep-1)*stdDesv/sqrt(Param.numRep)
    resultsDF.loc['Mean'] = mean
    resultsDF.loc['Std Desv'] = stdDesv
    resultsDF.loc['IC inf @ 95%'] = mean - IC
    resultsDF.loc['IC sup @ 95%'] = mean + IC
    print("\n")
    print(resultsDF)
    
"""
Main simulation script
"""
print('\n o/-<]: Simulate or Die Skateboard Factory  Inc. o\-<]: \n')
    
random.seed(1000)
plt.close("all")
ax = Graph('Demand and inventory')

procList = ['cutting', 'pressing', 'foundry', 'assemble']
resultsDFCount = DataFrame(columns=procList, index=['Replicação ' + str(i) for i in range(Param.numRep)])
resultsDFTime = DataFrame(columns=procList, index=['Replicação ' + str(i) for i in range(Param.numRep)])

for i in range(Param.numRep):
    env = simpy.Environment()
    factory = Factory(env, trace=Param.trace)
    pedidos = Pedidos(env, factory)
    env.run(until=Param.simTime)
    
    if env.now >= Param.warmupTime:
        for j in procList:
            resultsDFCount.ix[i][j] = factory.procCount[j]
            resultsDFTime.ix[i][j] = factory.procTime[j]/factory.procCount[j]
    
    ax.plotPlt(pedidos.monitorList)

printResults(resultsDFCount)
printResults(resultsDFTime)


   
