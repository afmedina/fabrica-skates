# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 15:12:41 2016

@author: User
"""

from xl import Workbook #pyvot para trabalhar melhor com excel

class excelWriter(object):
    """Excel class for creating spreadsheets - esp writing data and formatting them
    Based in part on #http://snippets.dzone.com/posts/show/2036,
    and http://www.markcarter.me.uk/computing/python/excel.html
    """
    def __init__(self, file_name, make_visible=False):
        """Open spreadsheet"""
        self.excelapp = Workbook(file_name)
        if make_visible:
            self.excelapp.Visible = 1 #fun to watch!

        self.file_name = file_name
        self.wb = self.excelapp.xlWorkbook
        self.default_sheet = self.wb.ActiveSheet
        
    def getExcelApp(self):
        """Get Excel App for use"""
        return self.excelapp
    
    def addSheet(self, sheet_name):
        """Add new sheet to workbook"""
        sheets = self.wb.Worksheets
        sheets.Add().Name = sheet_name #Sheets.Add(Before, After, Count, Type) - http://www.functionx.com/vbaexcel/Lesson07.htm
    
    def deleteSheet(self, sheet_name):
        """Delete named sheet"""
        #http://www.exceltip.com/st/Delete_sheets_without_confirmation_prompts_using_VBA_in_Microsoft_Excel/483.html
        sheets = self.wb.Sheets
        self.wb.DisplayAlerts = False        
        sheets(sheet_name).Delete()        
        self.wb.DisplayAlerts = True
        
    def getSheet(self, sheet_name):
        """
        Get sheet by name.
        """
        return self.wb.Sheets(sheet_name)
    
    def activateSheet(self, sheet_name):
        """
        Activate named sheet.
        """
        sheets = self.wb.Sheets
        sheets(sheet_name).Activate() #http://mail.python.org/pipermail/python-win32/2002-February/000249.html
    
    def add2cell(self, row, col, content, sheet=None):
        """
        Add content to cell at row,col location.  
        NB only recommended for small amounts of data http://support.microsoft.com/kb/247412.
        """
        if sheet == None:
            sheet = self.default_sheet
        sheet.Cells(row,col).Value = content
        
    def addRow(self, row_i, data_tuple, start_col=1, sheet=None):
        """
        Add row in a single operation.  Takes a tuple per row.
        Much more efficient than cell by cell. http://support.microsoft.com/kb/247412.
        """
        if sheet == None:
            sheet = self.default_sheet  
        col_n = len(data_tuple)
        last_col = start_col + col_n - 1
        insert_range = self.getRangeByCells((row_i, start_col), (row_i, last_col), sheet)
        insert_range.Value = data_tuple
        
    def addMultipleRows(self, start_row, list_data_tuples, start_col=1, sheet=None):
        """
        Adds data multiple rows at a time, not cell by cell. Takes list of tuples
        e.g. cursor.fetchall() after running a query
        One tuple per row.
        Much more efficient than cell by cell or row by row. 
        http://support.microsoft.com/kb/247412.
        Returns next available row.
        """
        if sheet == None:
            sheet = self.default_sheet
        row_n = len(list_data_tuples)
        last_row = start_row + row_n - 1
        col_n = len(list_data_tuples[0])
        last_col = start_col + col_n - 1        
        insert_range = self.getRangeByCells((start_row, start_col), (last_row, last_col), sheet)
        insert_range.Value = list_data_tuples
        next_available_row = last_row + 1
        return next_available_row

    def eraseCol(self, start_col, last_col, sheet):
        """ Erase all data in a range of columns """
        for col in range(start_col, last_col+1):
            sheet.Columns(col).Cells.ClearContents()

    def eraseCells(self, sheet):
        """ Erase all data in a cells """
        sheet.UsedRange.ClearContents()
            
    def getRangeByCells(self, a, b, sheet=None):
        """Get a range defined by cell start and cell end e.g. (1,1) A1 and (7,2) B7"""
        (cell_start_row, cell_start_col) = a
        (cell_end_row, cell_end_col) = b
        if sheet == None:
            sheet = self.default_sheet
        return sheet.Range(sheet.Cells(cell_start_row, cell_start_col), 
            sheet.Cells(cell_end_row, cell_end_col))
    
    def fitCols(self, col_start, col_end, sheet=None):
        """
        Fit colums to contents.
        """
        if sheet == None:
            sheet = self.default_sheet
        col_n = col_start
        while col_n <= col_end:
            self.fitCol(col_n, sheet)
            col_n = col_n + 1
    
    def fitCol(self, col_n, sheet=None):
        """
        Fit column to contents.
        """
        if sheet == None:
            sheet = self.default_sheet
        sheet.Range(sheet.Cells(1, col_n), sheet.Cells(1, col_n)).EntireColumn.AutoFit()
    
    def setColWidth(self, col_n, width, sheet=None):
        """
        Set column width.
        """
        if sheet == None:
            sheet = self.default_sheet
        sheet.Range(sheet.Cells(1, col_n), sheet.Cells(1, col_n)).ColumnWidth = width        

    def mergeRange(self, range):
        """Merge range"""
        range.Merge()

    def save(self):
        """Save spreadsheet as filename - wipes if existing"""
        self.wb.Save
        
    def close(self):
        """Close spreadsheet resources"""
        self.wb.Saved = 0 #p.248 Using VBA 5
        self.wb.Close(SaveChanges=0) #to avoid prompt
        del self.excelapp
        del self.wb
        
def carregaParam(Param):
    """carega parametros da simulacao"""
    wb = excelWriter(Param.arqInterface, make_visible=True)
    
