# -*- coding: utf-8 -*-
import simpy  # call simpy framework
import random # module for random distributions

# defaults process times
procTimeDict = {'pressing': 60.0, 'cutting': 40.0, 'foundry': 50.0, 'assemble': 30.0, 'packing deck': 15.0, 'packing wheel': 12.0}


def simDistributions(dist):
   return {
        'pressing': random.normalvariate(procTimeDict['pressing'], .1*procTimeDict['pressing']),
        'cutting': random.normalvariate(procTimeDict['cutting'], .1*procTimeDict['cutting']),
        'foundry': random.normalvariate(procTimeDict['foundry'], .1*procTimeDict['foundry']),
        'assemble': random.normalvariate(procTimeDict['assemble'], .1*procTimeDict['assemble']),
        'packing deck': random.normalvariate(procTimeDict['packing deck'], .1*procTimeDict['packing deck']),
        'packing wheel': random.normalvariate(procTimeDict['packing wheel'], .1*procTimeDict['packing wheel']),
    }.get(dist, 0.0) 
            
class Factory(object):
    def __init__(self, env, trace=False, hours=8):
        self.env = env
        self.working = hours*60
        self.notWorking = 24*60 - self.working
        self.log = trace
        self.nextDay, self.endDay = self.env.event(), self.env.event()
        self.WIPTotal = 0
        self.WIP, self.procDict, self.procRes, self.inventory, self.queue, self.procCount, self.procTime = {}, {}, {}, {}, {}, {}, {}
        if self.log:
            print("{0:^5}   {1:^20}   {2:^10}   {3:^6}   {4:^6}   {5:^6}".format('  Time', 'Event', 'Local', 'Queue', 'Count', 'WIPtot'))
            
    def addProcess(self, process):
        def addInventory(i):
            if not i in self.inventory:
                self.inventory[i] = simpy.Container(self.env)
                self.inventory[i+'.tmp'] = simpy.Container(self.env) 
                    
        self.procDict[process.pKey], self.WIP[process.pKey], self.queue[process.pKey], self.procCount[process.pKey], self.procTime[process.pKey] = 0, 0, 0, 0, 0

        if not process.pKey in self.procRes:
            self.procRes[process.pKey] = simpy.PriorityResource(self.env, capacity=process.numRes)
            
        if process.getKey:
            for i in process.getKey:
                addInventory(i)
        if process.putKey:    
            addInventory(process.putKey)
                
    def workingDay(self):
        while True:
            yield self.env.timeout(self.working)     
            self.endDay.succeed()
            self.endDay = self.env.event()
            yield self.env.timeout(self.notWorking)
            self.nextDay.succeed()
            self.nextDay = self.env.event()

    def turnResOff(self, pKey):
        while True:
            yield self.endDay
            req = self.procRes[pKey].request(priority=-1) 
            yield req
            yield self.nextDay
            yield self.procRes[pKey].release(req)
            
    def upWip(self, processKey, begin=False):
    # incrementa o WIP
        self.WIP[processKey] += 1
        if begin:
            self.WIPTotal += 1
            
    def downWip(self, processKey, end=False):
    # incrementa o WIP
        self.procCount[processKey] += 1 
        self.WIP[processKey] -= 1 
        if end:
            self.WIPTotal -= 1
    
    def queueSize(self, getKey):
        return sum([self.inventory[k].level for k in getKey])
        
    def pullProcess(self, currProcess):    
        while True:
            with self.procRes[currProcess.pKey].request(priority=0) as req:
                putInv = self.inventory[currProcess.putKey + '.tmp'].put(1)                                
                getInv = [self.inventory[k].get(1) for k in currProcess.getKey]
                yield simpy.AllOf(self.env, getInv) & putInv & req
                startTime = self.env.now
                self.queue[currProcess.pKey] = self.queueSize(currProcess.getKey)
                for k in currProcess.getKey:
                    self.inventory[k + '.tmp'].get(1)
                    self.logSimula(currProcess.pKey, (k + '-', currProcess.local))                    

                self.upWip(currProcess.pKey, currProcess.begin)
                self.logSimula(currProcess.pKey, ('START '+ currProcess.pKey, currProcess.local))
                yield self.env.timeout(simDistributions(currProcess.pKey))
                
            self.procTime[currProcess.pKey] += self.env.now - startTime      
            self.downWip(currProcess.pKey, currProcess.end)
            self.queue[currProcess.pKey] = self.queueSize(currProcess.getKey)
            self.logSimula(currProcess.pKey, ('END '+ currProcess.pKey, currProcess.local))
            
            if currProcess.wait == True:
                self.logSimula(currProcess.pKey, ('START WAIT '+ currProcess.pKey, currProcess.local))
                yield self.nextDay
                self.logSimula(currProcess.pKey, ('END WAIT '+ currProcess.pKey, currProcess.local))

            yield self.inventory[currProcess.putKey].put(1)
            self.logSimula(currProcess.pKey, (currProcess.putKey + '+', currProcess.local))

    def pushProcess(self, currProcess):
        self.queue[currProcess.pKey] += 1
        with self.procRes[currProcess.pKey].request(priority=0) as req:
            putInv = self.inventory[currProcess.putKey + '.tmp'].put(1)
            if currProcess.getKey:
                getInv = [self.inventory[k].get(1) for k in currProcess.getKey]
                yield simpy.AllOf(self.env, getInv) & putInv & req
               
                for k in currProcess.getKey:
                    self.inventory[k + '.tmp'].get(1)
                    self.logSimula(currProcess.pKey, (k + '-', currProcess.local))
            else:
                yield putInv & req
                
            startTime = self.env.now   
            self.queue[currProcess.pKey] -= 1
            self.upWip(currProcess.pKey, currProcess.begin)
            self.logSimula(currProcess.pKey, ('START '+ currProcess.pKey, currProcess.local))
            yield self.env.timeout(simDistributions(currProcess.pKey))
            self.downWip(currProcess.pKey, currProcess.end)
            self.logSimula(currProcess.pKey, ('END '+ currProcess.pKey, currProcess.local))
        
        self.procTime[currProcess.pKey] += self.env.now - startTime
        if currProcess.wait == True:
            self.logSimula(currProcess.pKey, ('START WAIT '+ currProcess.pKey, currProcess.local))
            yield self.nextDay
            self.logSimula(currProcess.pKey, ('END WAIT '+ currProcess.pKey, currProcess.local))
            
        yield self.inventory[currProcess.putKey].put(1)
        self.logSimula(currProcess.pKey, (currProcess.putKey + '+', currProcess.local))
        
        if currProcess.nextProcess:
            self.env.process(self.pushProcess(currProcess.nextProcess))
                  
    def logSimula(self, processKey, txt):
        def day_hours_minutes(now):
            day = int(now//1440)
            hour = int((now%1440)//60)
            minute = int((now%1440)%60)
            zero = ("0" if hour < 10 else "", "0" if minute < 10 else "")
            return ("%s %s%s:%s%s" %(day, zero[0], hour, zero[1], minute))
            
        if self.log:
            print("{0:^10}   {1:20}   {2:10}  {3:6}   {4:6}   {5:6}".format(day_hours_minutes(self.env.now), txt[0][:20].upper(), txt[1][:12].upper(), self.queue[processKey], self.procCount[processKey],  self.WIPTotal ))
 

