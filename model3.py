# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:56:14 2016

@author: afmedina2gmail.com
"""
# -*- coding: utf-8 -*-

import random # gerador de números aleatórios
import simpy  # biblioteca de simulação
from pandas import DataFrame
from scipy.stats import t
from numpy import sqrt
import matplotlib.pyplot as plt

from factory import Factory

demandRateDict = {'skates demand': 8, 'decks demand': 6, 'wheels demand': 6}

def demandRates(dist):
    # main demand distributions
    return {
        'skates demand': random.randint(0, demandRateDict['skates demand']),
        'decks demand': random.randint(0, demandRateDict['decks demand']),
        'wheels demand': random.randint(0, demandRateDict['wheels demand']),
    }.get(dist, 0.0)

class Param:
    # main simulation parameters
    simTime = 101.01*24*60  # total simulation time (days)
    numRep = 1              # number of replication
    warmupTime = 0          # warm-up time (days)
    trace = False           # simulation process trace
    model = 5               # run model
    
   
class Process():
    def __init__(self, factory, name, local, numRes=1, getKey=None, putKey=None, wait=False, nextProcess=None, route=None, begin=False, end=False):
        self.pKey = name
        self.local = local
        self.numRes = numRes
        self.getKey = getKey
        self.putKey = putKey
        self.wait = wait
        self.nextProcess = nextProcess
        self.route = None
        self.begin, self.end = begin, end
        factory.addProcess(self)
           
class Pedidos(object):
    def __init__(self, env, factory):
        self.env = env
        self.monitorList = []
        self.dailyDemand = tuple()
        self.env.process(self.createModel(factory)) # create and start model's simulations process
        self.env.process(self.monitor(60))            # start monitor simulation process
        self.env.process(factory.workingDay())          # start working schedulle simulation process
        for res in factory.procRes.values():
            self.env.process(factory.turnResOff(res))   # start open/closed simulation process
       
    def createModel(self, factory):
        cutting_deck = Process(factory, 'cutting', 'Deck', getKey=['storage 1'], putKey='storage 2a', wait=True)
        cutting_skate = Process(factory,'cutting', 'Deck', getKey=['storage 1'], putKey='storage 2b', wait=True)              
        pressing_deck = Process(factory,'pressing', 'Deck', putKey='storage 1', wait=True, begin=True, nextProcess=cutting_deck)
        pressing_skate = Process(factory,'pressing', 'Deck', putKey='storage 1', wait=True, begin=True, nextProcess=cutting_skate)
        foundry_wheel = Process(factory,'foundry', 'Wheel', putKey='storage 3a', begin=True, wait=True)
        foundry_skate = Process(factory,'foundry', 'Wheel', putKey='storage 3b', wait=True)
        packing_1 = Process(factory,'packing deck', 'Deck', getKey=['storage 2a'], end=True, putKey='decks')
        packing_2 = Process(factory,'packing wheel', 'Wheel', getKey=['storage 3a'], end=True, putKey='wheels')
        assemble = Process(factory,'assemble', 'Skateboard', getKey=['storage 2b', 'storage 3b'], putKey='skates', end=True)
        
        yield factory.inventory['skates'].put(10)
        yield factory.inventory['decks'].put(6)
        yield factory.inventory['wheels'].put(6)
        
        demandRateDict['skates demand'] = 8
        demandRateDict['decks demand'] = 2
        demandRateDict['wheels demand'] = 3
        
        self.env.process(factory.pullProcess(packing_1))
        self.env.process(factory.pullProcess(packing_2))
        self.env.process(factory.pullProcess(assemble))
        

        while True:
            prodOrderA = min(demandRateDict['skates demand'], factory.inventory['skates'].level)
            if prodOrderA > 0:
                yield factory.inventory['skates'].get(prodOrderA)
                for i in range(prodOrderA):
                    self.env.process(factory.pushProcess(pressing_skate))
                    simpy.util.start_delayed(self.env, factory.pushProcess(foundry_skate), 1440)
            
            prodOrderB = min(demandRateDict['decks demand'], factory.inventory['decks'].level)
            if prodOrderB > 0:
                yield factory.inventory['decks'].get(prodOrderB)
                for i in range(prodOrderB):
                    self.env.process(factory.pushProcess(pressing_deck))
                
            prodOrderC = min(demandRateDict['wheels demand'], factory.inventory['wheels'].level)
            if prodOrderC > 0:
                yield factory.inventory['wheels'].get(prodOrderC)
                for i in range(prodOrderC):
                    self.env.process(factory.pushProcess(foundry_wheel))
            self.dailyDemand = (prodOrderA, prodOrderB, prodOrderC)
            
            yield factory.nextDay

    def monitor(self, interval=1440):
        while True:
            yield env.timeout(interval)
            self.monitorList.append((self.env.now/1440.0,
            factory.inventory['skates'].level, factory.inventory['decks'].level, factory.inventory['wheels'].level,
            factory.WIPTotal, self.dailyDemand))

class Graph(object):
    def __init__(self, title):
        self.title = title
        self.fig = plt.figure()
        self.ax = plt.subplot(111)
        self.ax.grid(True)
        self.fig.canvas.set_window_title(self.title)
        
    def plotPlt(self, data):
        x, y, z, w, t, d = zip(*data)
        d1, d2, d3 = zip(*d)
        self.ax.plot(x, y, label='Skates inventory', linewidth=2.0)
        self.ax.plot(x, z, label='Decks inventory', linewidth=2.0)
        self.ax.plot(x, w, label='Wheels inventory', linewidth=2.0)
        self.ax.plot(x, t, label='WIP', linewidth=2.0)
        self.ax.plot(x, d1, label='Skates demand ', linewidth=2.0)
        self.ax.plot(x, d2, label='Wheel demand ', linewidth=2.0)
        self.ax.plot(x, d3, label='Deck demand ', linewidth=2.0)
        self.ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.show()
    
def printResults(resultsDF):
    mean = resultsDF.mean()
    stdDesv = resultsDF.std()
    IC = t.ppf(0.975, Param.numRep-1)*stdDesv/sqrt(Param.numRep)
    resultsDF.loc['Mean'] = mean
    resultsDF.loc['Std Desv'] = stdDesv
    resultsDF.loc['IC inf @ 95%'] = mean - IC
    resultsDF.loc['IC sup @ 95%'] = mean + IC
    print("\n")
    print(resultsDF)
    
"""
Main simulation script
"""
print('\n o/-<]: Simulate or Die Skateboard Factory  Inc. o\-<]: \n')
    
random.seed(1000)
plt.close("all")
ax = Graph('Demand and inventory')

procList = ['cutting', 'pressing', 'foundry', 'packing deck', 'packing wheel', 'assemble']
resultsDFCount = DataFrame(columns=procList, index=['Replicação ' + str(i) for i in range(Param.numRep)])
resultsDFTime = DataFrame(columns=procList, index=['Replicação ' + str(i) for i in range(Param.numRep)])

for i in range(Param.numRep):
    env = simpy.Environment()
    factory = Factory(env, trace=Param.trace)
    pedidos = Pedidos(env, factory)
    env.run(until=Param.simTime)
    
    if env.now >= Param.warmupTime:
        for j in procList:
            resultsDFCount.ix[i][j] = factory.procCount[j]
            resultsDFTime.ix[i][j] = factory.procTime[j]/factory.procCount[j]
            
    ax.plotPlt(pedidos.monitorList)

printResults(resultsDFCount)
printResults(resultsDFTime)


   
